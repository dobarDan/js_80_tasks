// 1.
// Write a JS program to display the current day and time like:
	// Output: Today is: Friday
	// Time is: 4 PM : 50 : 22

/* Tasks are from this url https://www.w3resource.com/javascript-exercises/javascript-basic-exercises.php*/

/*Usefully link https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Date*/


// Solution 1.1
/*Invoking JavaScript Date as a function (i.e., without the new operator) will return a string representing the current date and time.*/

/*
function getTodaysDateAndTime() {
	console.log(Date());
};

getTodaysDateAndTime();
// empty line inside terminal
console.log();

*/
/*
If no arguments are provided, the constructor creates a JavaScript Date object for the current date and time according to system settings.
*/



// Solution 1.2
// How to get a specific date
// let xmas95 = new Date('December 25, 2010 23:15:30');

/*
dateObj.getDay() > An integer number corresponding to the day of the week for the given date, according to local time: 0 for Sunday, 1 for Monday, 2 for Tuesday, and so on.
https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Date/getDay
*/

/*
function dayAndTime(argument) {
	// body...
	// How to get current day
	let todayDay = new Date();

	let weekDay = todayDay.getDay();

	if (weekDay === 0) {
		console.log('Today is Sunday')
	} else if (weekDay === 1) {
		console.log('Today is Monday')
	} else if (weekDay === 2) {
		console.log('Today is Tuesday')
	} else if (weekDay === 3) {
		console.log('Today is Wednesday')
	} else if (weekDay === 4) {
		console.log('Today is Thursday')
	} else if (weekDay === 5) {
		console.log('Today is Friday')
	} else if (weekDay === 6) {
		console.log('Today is Saturday')
	} 
					

	// Check the days return value (0-6)					
	// console.log(weekDay);


	// getTime() returns the time in milliseconds

	let currentHours = todayDay.getHours();
	let currentMinutes = todayDay.getMinutes();
	let currentSeconds = todayDay.getSeconds();
	console.log('- Time is ' + currentHours, ': ' + currentMinutes, ': ' + currentSeconds);
}

dayAndTime();

*/


// Solution 1.3
// I saw this solution on web and its easy

/*
function dayAndTime(argument) {
	// body...
	// How to get current day
	let todayDay = new Date();

	let weekDay = todayDay.getDay();
	const listDays = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
	console.log('- Today is a day ' + listDays[weekDay]);

	// This is pointless in solution 1.3
	if (weekDay === 0) {
		console.log('Today is Sunday')
	} else if (weekDay === 1) {
		console.log('Today is Monday')
	} else if (weekDay === 2) {
		console.log('Today is Tuesday')
	} else if (weekDay === 3) {
		console.log('Today is Wednesday')
	} else if (weekDay === 4) {
		console.log('Today is Thursday')
	} else if (weekDay === 5) {
		console.log('Today is Friday')
	} else if (weekDay === 6) {
		console.log('Today is Saturday')
	} 				

	// Check the days return value (0-6)					
	// console.log(weekDay);


	// getTime() returns the time in milliseconds


	let currentHours = todayDay.getHours();
	let currentMinutes = todayDay.getMinutes();
	let currentSeconds = todayDay.getSeconds();
	console.log('- Time is ' + currentHours, ': ' + currentMinutes, ': ' + currentSeconds);
}

dayAndTime();
*/












// 2.
// Write a JavaScript program to print the contents of the current window
/*
Links:
https://www.w3schools.com/jsref/tryit.asp?filename=tryjsref_print
https://www.w3schools.com/jsref/met_win_print.asp
https://developer.mozilla.org/en-US/docs/Web/API/Window/print
*/


/*
// Solution 2.1

function clickPrint() {
	window.print();
};
// Just to see console function output
console.log(clickPrint);
*/















// 3.
// Write a JavaScript program to get the current date
// Expected Output : dd/mm/yyyy



// Solution 3.1
// current
let getDate = new Date;

// This is from the official solution. I could have used this to return date
// let izvuciFunkcijuGetDate = getDate.getDate();
// console.log('- Izloguj mi povratak funkcije getDate()', izvuciFunkcijugetDate);


let findDay = 0;
let findMounth = 0;
let findYear = 0;
let dayName = 0;
let mounthName = 0;

const weekDays = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];

const mounthList = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

const dayNumbers = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24', '25', '26', '27', '28', '29', '30', '31'];

const mountNumbers = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12'];

	
function currentTime() {
	findDay = getDate.getDay();
	findMounth = getDate.getMonth();
	findYear = getDate.getFullYear();
	console.log('Output from getDate() is: ' ,getDate, ' some string', ' bla bla', getDate);

	dayName = weekDays[findDay];
	mounthName = mounthList[findMounth];

	dayNumber = dayNumbers[findDay];
	mountNumber = mountNumbers[findMounth];

	// console.log('return vrednost meseca je ' + findMounth)
}

currentTime();
// console.log(getDate);
console.log('- Returned values for date are: ' + findDay + '/' + findMounth + '/' + findYear);

console.log('- Day name is ' + dayName + ', Mounth name is ' + mounthName);

console.log('- Date is: ' + dayNumber + '/' + mountNumber + '/' + findYear);



/*
// Official solution
// https://www.w3resource.com/javascript-exercises/javascript-basic-exercise-3.php

var today = new Date();
var dd = today.getDate();

// +1 because the function starts counting months from 0
var mm = today.getMonth()+1; 
var yyyy = today.getFullYear();

// This is because getYear don't return full year
// var yyyy = today.getYear()+1900;

if(dd<10) 
{
    dd='0'+dd;
} 

if(mm<10) 
{
    mm='0'+mm;
} 
console.log();
today = mm+'-'+dd+'-'+yyyy;
console.log(today);
today = mm+'/'+dd+'/'+yyyy;
console.log(today);
today = dd+'-'+mm+'-'+yyyy;
console.log(today);
today = dd+'/'+mm+'/'+yyyy;
console.log(today);
*/



















// 4.
// Write a JavaScript function to find the area of a triangle where lengths of the three of its sides are 5, 6, 7.

/*Help on how to use function arguments:
JavaScript Video Tutorial - The Flexibility of Function Arguments in JavaScript
https://www.youtube.com/watch?v=G4lZSWssoqA
*/

// Note: I miss understood the task. It's a math task to find a area

// Solution 4.1
// OK program, but not for this task
/*
function findMatchSizes(a, b, c) {
	let sizeCheck = a + b + c;
	console.log('- The sum of sides you entered is ', sizeCheck);

	// Triangle sides are 5, 6, 7 and the sum of them is 18
	if (sizeCheck === 18) {
		console.log('- The exact size is 18. The triangle side sizes are matched');
	} else 
		console.log('- The exact size is 18. The triangle side sizes are different.', 'And the difference is: ', sizeCheck - 18);
};
findMatchSizes(5,6,7);
// findMatchSizes(5,6,8);
// findMatchSizes(5,6,'a');
*/


/*
// Solution 4.2
// This is edited solution so that it makes sense
function checkTheInput(a, b, c) {
	let valueSumInput = a + b + c;
	console.log('- The total of your numbers is ', valueSumInput);

	if (valueSumInput === 18) {
		console.log('- We expected 18. You entered', valueSumInput, 'The input is a match');
	} else 
		console.log('- We expected 18. You entered', valueSumInput,'.', 'No match.', 'The difference is: ', valueSumInput - 18);
};
checkTheInput(5,6,7);
// checkTheInput(5,6,8);
// checkTheInput(5,6,'a');
*/


/*
// Official solution for task 4
var side1 = 5; 
var side2 = 6; 
var side3 = 7; 
var perimeter = (side1 + side2 + side3)/2;
var area =  Math.sqrt(perimeter*((perimeter-side1)*(perimeter-side2)*(perimeter-side3)));
console.log(area);


// console.log(perimeter);
// console.log(perimeter-side1);
// console.log(area * area);
// console.log(perimeter-side2);
// console.log(perimeter-side3);

// let test = Math.sqrt(216);
// console.log(test);
*/


/*
// One way how to read a value from console

// Reading value from console, interactively
// https://stackoverflow.com/questions/8128578/reading-value-from-console-interactively
var stdin = process.openStdin();

stdin.addListener("data", function(d) {
    // note:  d is an object, and when converted to a string it will
    // end with a linefeed.  so we (rather crudely) account for that  
    // with toString() and then trim() 
    console.log("you entered: [" + 
        d.toString().trim() + "]");
  });
*/


// 5
/*Write a JavaScript program to rotate the string 'helloString' in right direction by periodically removing one letter from the end of the string and attaching it to the front.*/

/* Plan: 
- This is a terminal application
- Move string letter at every 3 seconds
- 
*/


/*
// some code from mozilla docks site
'use strict';
console.log(typeof 'use strict')

function splitString(stringToSplit, separator) {
  var arrayOfStrings = stringToSplit.split(separator);

  console.log('The original string is: "' + stringToSplit + '"');
  console.log('The separator is: "' + separator + '"');
  console.log('The array has ' + arrayOfStrings.length + ' elements: ' + arrayOfStrings.join(' / '));
}

var tempestString = 'Oh brave new world that has such people in it.';
var monthString = 'Jan,Feb,Mar,Apr,May,Jun,Jul,Aug,Sep,Oct,Nov,Dec';

var space = ' ';
var comma = ',';

splitString(tempestString, space);
splitString(tempestString);
splitString(monthString, comma);
*/






























